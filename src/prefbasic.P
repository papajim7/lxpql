:- import delete/3 from listutil.
:- hilog iscallableexp, iscallable, getindex, getminmax, foldl, map, select, maxnum,maxn.


% maxnum(:X, :Y, :M)
%  M = max(X,Y).
maxnum(X,Y,X):-X>Y,!.
maxnum(_,Y,Y).

maxn(A,B):-A>B.

%
% select (:Op)(:X, :Y, :S)
%  if Op(X,Y) is true then S=X else S=Y.
select(Op)(X,_,X) :- catch(Op,_,fail), !.
select(Op)(X,Y,X) :- catch(Op(X,Y),_,fail), !.
select(_)(_,Y,Y).

% same as select/4 except that check with Op, is between UnFold(X) and UnFold(Y).
select(Op)(UnFold)(X,Y,X) :- UnFold(X,X1), UnFold(Y,Y1), Op(X1,Y1),!.
select(_)(_)(_,Y,Y).

%
% map operation

map(_)([],[]) :-!.
map(M)([X1|L1], [X2|L2]) :- !, M(X1,X2), map(M)(L1, L2).
map(M)(X1, X2) :- M(X1,X2).

%
% fold operation

foldl(_)(Z, [], Z).
foldl(F)(Z, [X|XL], V) :- F(Z,X,V1), foldl(F)(V1, XL, V).

% getminmax(:MinMax)(:L,:M).
%  M is the MinMax of list L

getminmax(_)([M],M).
getminmax(MinMax)([X|L],M):-getminmax(MinMax)(L, ML), MinMax(X, ML, M).


% ordindex(:MinMax)(:L, :I, :V).
%  V is the Ith item (according to MinMax) of list L.

ordindex(_)([],_,_):-fail.
ordindex(MinMax)(L, 1, V):-getminmax(MinMax)(L, V).
ordindex(MinMax)(L, I, V):-getminmax(MinMax)(L, M), delete(M, L, L1), I1 is I-1, ordindex(MinMax)(L1, I1, V).


% iscallable(:Exp)
%  true if Exp is callable (for example, if there is no predicate foo(X), then iscallable(foo(X)) fails). 
iscallable(E) :- catch(clause(E,_), _, iscallableexp(X)), X=t,!.
iscallable(E) :- catch(clause(E,_), _, fail).
iscallableexp(t).